# 1ère Histoire: L’Europe face aux révolutions

## Titre niveau 2 L'Europe entre restauration et Révolution 1814-1848

### 1 sous-titre niveau 3 Introduction

Après la défaite de Napoléon en 1815, la peur que des mouvements révolutionnaires ressurgissent incite les monarchies européennes à agir : les monarques vainqueurs de Napoléon se réunissent au congrès de Vienne (1814-1815) pour tenter d’effacer l’empreinte de la Révolution française et de l’Empire napoléonien .​

Ils effectuent une restauration de l’ordre ancien.
Mais ces monarques ne veulent pas tenir compte des aspirations des peuples à disposer d’eux- mêmes (notamment leur volonté de créer un État pour chaque nation). Par deux fois, en 1830 et en 1848, l’équilibre monarchique est ébranlé par des révolutions (soulèvements violents d’une population qui renversent l’ordre politique et social) en France et en Europe.

### 2 sous-titre niveau 3

Problématique:  
Pourquoi et comment la restauration de l'ordre monarchique en France et et Europe ne parvient à contenir l'essor des mouvements libéraux et nationaux ?

## Titre niveau 2 Un nouvel ordre européen en 1815: l'ordre des princes contre l'ordre des peuples

### Le Congrès de Vienne rétablit l'ordre...

![croquis Europe 1815](./img/Croquis Europe congrès de Vienne.png)

### ...mais n'empêche pas le développement des nationalités...

![Giuseppe MAZZINI​, (1805-1872)​](./img/G.Mazzini.jpg)

Né le 22 juin 1805 à Gênes et mort le 10 mars 1872 à Pise, c'est un révolutionnaire et patriote italien, fervent républicain et combattant pour la réalisation de l’unité italienne.​

 Il est considéré avec Giuseppe Garibaldi, Victor-Emmanuel II et Camillo Cavour, comme l’un des « pères de la patrie ». Mazzini a participé et soutenu tous les mouvements insurrectionnels en Italie qui se sont avérés pour leur grande majorité des échecs mais son action a eu pour effet d’ébranler les petits États de la péninsule et d’inquiéter les plus grands comme le Royaume de Sardaigne, puis le Royaume d’Italie à partir de 1861, la France et l’Empire d’Autriche dont Metternich, Premier ministre autrichien, dit de lui : « J’ai dû lutter avec le plus grand des soldats, Napoléon. Je suis arrivé à mettre d’accord entre eux les empereurs, les rois et les papes. Personne ne m’a donné plus de tracas qu’un brigand italien : maigre, pâle, en haillons, mais éloquent comme la tempête, brûlant comme un apôtre, rusé comme un voleur, désinvolte comme un comédien, infatigable comme un amant, qui a pour nom : Giuseppe Mazzini. ». ​

Ses idées et son action politique ont largement contribué à la naissance de l’État unitaire italien alors que les condamnations des différents tribunaux de l’Italie l’ont forcé à l’exil et la clandestinité jusqu’à sa mort. Les théories mazziniennes sont d’une grande importance dans la définition du mouvement moderne européen par l’affirmation de la démocratie à travers la forme républicaine de l’État. ​

En politique italienne, il constitue une référence permanente, ce qui lui a valu d’être récupéré par toutes les tendances politiques : le fascisme, la résistance et sa famille républicaine.

!!! col _2
    texte 1

!!! col _2
    texte 2

<iframe width="560" height="315" src="https://www.youtube.com/embed/OMvo2PX4_l0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>