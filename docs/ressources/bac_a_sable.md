# 1HISTOIRE : Bac à Sable

<bd grey>Badge "Normal" Statique Gris</bd>
<bd @grey>Badge "Normal" Statique Gris</bd>

<bd gris>Badge "Normal" Statique Gris</bd>
<bd @gris>Badge "Normal" Statique Gris</bd>

```mermaid
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```

Tableaux :

!!! col __40
    |colonne A|colonne B|
    |:-:|:-:|
    |Cellule 1 { linestyle="test" } | Cellule 2 {cell="chartreuse"}  |
    |Cellule 3| Cellule 4|
    |Cellule 5| Cellule 6 { col="blue"} |
    |Cellule 7| Cellule 8|
    {tablestyle="test"}
!!! col __60
    |colonne A|colonne B|colonne B|
    |:-:|:-:|:-:|
    | Cellule 1 {.ligne=""} | Cellule 2 |Cellule 3 |
    | > | Cellule 5 | Cellule 6 |
    | Cellule 7 | Cellule 8 | Cellule 9 |
    | ^ | Cellule 11 | Cellule 12 |

<iframe width="600" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" sandbox="allow-forms allow-scripts allow-same-origin" src="https://www.geoportail.gouv.fr/embed/visu.html?c=5.947995457793214,45.251014060609464&z=7&l0=OPEN_STREET_MAP::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.MAPS.BDUNI.J1::GEOPORTAIL:OGC:WMTS(1)&v2=PLAN.IGN::GEOPORTAIL:GPP:TMS(1;s:standard)&l3=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes" allowfullscreen></iframe>
<iframe width="100%" height="600px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-4.471435546875001%2C41.9921602333763%2C10.030517578125002%2C49.25346477497736&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=7/45.741/2.780">Afficher une carte plus grande</a></small>


